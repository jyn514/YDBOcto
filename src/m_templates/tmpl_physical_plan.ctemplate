{}%
/****************************************************************
 *								*
 * Copyright (c) 2019-2020 YottaDB LLC and/or its subsidiaries.	*
 * All rights reserved.						*
 *								*
 *	This source code contains the intellectual property	*
 *	of its copyright holder(s), and is made available	*
 *	under a license.  If you do not know the terms of	*
 *	the license, please stop and do not read further.	*
 *								*
 ****************************************************************/

#include "physical_plan.h"
#include "template_helpers.h"

TEMPLATE(tmpl_physical_plan, PhysicalPlan *plan) {
	TEMPLATE_INIT();

	int			i;
	SqlKey			*key, *t_key, *prev_t_key;
	SqlColumnAlias		*column_alias;
	SqlValue		*value;
	LogicalPlan		*lp_temp;
	char			*tableName = "";
	char			*columnName = "";
	unsigned int		cur_key;
	int			dot_count;
	char			*plan_helper_mlabref;
	SetOperType		*set_oper, *prev_oper;
	SqlOptionalKeyword	*limit_keyword;

	%{}`n{{ plan->plan_name }}(cursorId)`n{{ PLAN_LINE_START }}{}% // The whitespace here is needed for proper M formatting

	// The below initialization is needed for cross-reference plans and the regular plans
	assert(NULL == plan->treat_key_as_null);
	plan->treat_key_as_null = octo_cmalloc(memory_chunks, sizeof(boolean_t) * config->plan_id);
	assert(NULL != plan->outputKey);
	// Check if there are any cross references that need to be built
	if (plan->outputKey->is_cross_reference_key) {
		UNPACK_SQL_STATEMENT(value, plan->outputKey->table->tableName, value);
		tableName = value->v.reference;
		UNPACK_SQL_STATEMENT (value, plan->outputKey->column->columnName, value);
		columnName = value->v.reference;
		GET_LP(lp_temp, plan->projection, 0, LP_WHERE);
		GET_LP(lp_temp, lp_temp, 0, LP_COLUMN_ALIAS);
		column_alias = lp_temp->v.lp_column_alias.column_alias;
		%{}LOCK +^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}"):10`n{{ PLAN_LINE_START }}{}%
		%{}IF $GET(^{{ config->global_names.raw_octo }}("xref_status","{{ tableName }}","{{ columnName }}"))="done" {}%
		// If this a plan for a cross reference, put safeguards in place (i.e. QUIT) to prevent building twice.
		%{}LOCK -^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}") QUIT`n{{ PLAN_LINE_START }}{}%
		%{}IF '$$dollarZTRIGGER^%%ydboctoplanhelpers("item","+{}%
		TMPL(tmpl_column_reference, plan, column_alias, TRUE);
		%{} -commands=SET,KILL,ZKILL -xecute=""DO handleSet^{{ plan->filename }}"" -name={{ plan->trigger_name }}")  {}%
		%{}HALT`n{{ PLAN_LINE_START }}{}%
		%{}SET %%ydboctoCancel("{{ tableName }}","{{ columnName }}","Trigger")="-{}%
		TMPL(tmpl_column_reference, plan, column_alias, TRUE);
		%{} -commands=SET,KILL,ZKILL -xecute=""DO handleSet^{{ plan->filename }}"""`n{{ PLAN_LINE_START }}{}%
		%{}SET %%ydboctoCancel("{{ tableName }}","{{ columnName }}","Node")="^{{ config->global_names.raw_xref }}(""{}%
		%{}{{ tableName }}"",""{{ columnName }}"")"`n{{ PLAN_LINE_START }}{}%
		%{}DO populateXref`n{{ PLAN_LINE_START }}{}%
		%{}SET ^{{ config->global_names.raw_octo }}("xref_status","{{ tableName }}","{{ columnName }}")="done"{}%
		%{}`n{{ PLAN_LINE_START }}{}%
		%{}KILL %%ydboctoCancel`n{{ PLAN_LINE_START }}{}%
		%{}LOCK -^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}"){}%
		%{}`n{{ PLAN_LINE_START }}QUIT{}%
		// Generate the populateXref entryref here
		%{}`npopulateXref`n{{ PLAN_LINE_START }}{}%
		cur_key = 0;
		dot_count = 0;
		TMPL(tmpl_tablejoin, plan, plan->tablejoin, cur_key, FALSE, dot_count, tableName, columnName);
		%{}`n{{ PLAN_LINE_START }}QUIT{}%
		// Generate the trigger definitions here
		%{}`nhandleSet`n{{ PLAN_LINE_START }}{}%
		%{}NEW t`n{{ PLAN_LINE_START }}{}%
		%{}IF $ZTRIGGEROP'="S" DO`n{{ PLAN_LINE_START }}{}%	/* Note: '="S" check is to account for KILL or ZKILL */
		%{}. SET val=""`n{{ PLAN_LINE_START }}{}%
		%{}. FOR  SET val=$ORDER(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",val)) {}%
		%{}QUIT:val=""  DO:$DATA(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",val{}%
		lp_temp = plan->projection->v.lp_default.operand[1];
		i = 0;
		while (NULL != lp_temp) {
			%{},sub{{ i|%d }}{}%
			lp_temp = lp_temp->v.lp_default.operand[1];
			i++;
		}
		%{}))'=0`n{{ PLAN_LINE_START }}{}%
		%{}. . KILL ^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",val{}%
		lp_temp = plan->projection->v.lp_default.operand[1];
		i = 0;
		while (NULL != lp_temp) {
			%{},sub{{ i|%d }}{}%
			lp_temp = lp_temp->v.lp_default.operand[1];
			i++;
		}
		%{})`n{{ PLAN_LINE_START }}{}%
		%{}. . KILL:$INCREMENT(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",val),-1)=0 ^{}%
		%{}{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",val)`n{{ PLAN_LINE_START }}{}%
		%{}. . KILL:$INCREMENT(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}"),-1)=0 ^{}%
		%{}{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}")`n{{ PLAN_LINE_START }}{}%
		%{}ELSE  DO`n{{ PLAN_LINE_START }}{}%
		%{}. SET newValue=$PIECE($ZTVALUE,$ZTDELIM,$ZTUPDATE)`n{{ PLAN_LINE_START }}{}%
		%{}. SET oldValue=$PIECE($ZTOLDVAL,$ZTDELIM,$ZTUPDATE)`n{{ PLAN_LINE_START }}{}%
		%{}. KILL:oldValue'="" ^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",oldValue{}%
		lp_temp = plan->projection->v.lp_default.operand[1];
		i = 0;
		while (NULL != lp_temp) {
			%{},sub{{ i|%d }}{}%
			lp_temp = lp_temp->v.lp_default.operand[1];
			i++;
		}
		%{}){}%
		// Now decrement the counts for the globals
		%{}`n{{ PLAN_LINE_START }}{}%
		%{}. IF oldValue'="" {}%
		%{}IF $SELECT($INCREMENT(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",oldValue),-1){}%
		%{}:$INCREMENT(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}"),-1),{}%
		%{}1:$INCREMENT(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}"),-1)){}%
		%{}`n{{ PLAN_LINE_START }}{}%
		%{}. KILL:(oldValue'="")&(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",oldValue)=0) {}%
		%{}^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",oldValue)`n{{ PLAN_LINE_START }}{}%
		%{}. SET:newValue'="" ^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",newValue{}%
		lp_temp = plan->projection->v.lp_default.operand[1];
		i = 0;
		while (NULL != lp_temp) {
			%{},sub{{ i|%d }}{}%
			lp_temp = lp_temp->v.lp_default.operand[1];
			i++;
		}
		%{})=""{}%
		// Now decrement the counts for the globals
		%{}`n{{ PLAN_LINE_START }}{}%
		%{}. IF newValue'="" {}%
		%{}IF $SELECT($INCREMENT(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}",newValue)){}%
		%{}:$INCREMENT(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}")),{}%
		%{}1:$INCREMENT(^{{ config->global_names.raw_xref }}("{{ tableName }}","{{ columnName }}")))`n{{ PLAN_LINE_START }}{}%
		%{}QUIT{}%
		TEMPLATE_END();
		assert(FALSE);
	}
	// Not a cross-reference plan
	prev_t_key = NULL;
	for(cur_key = 0; cur_key < plan->total_iter_keys; cur_key++)
	{
		key = plan->iterKeys[cur_key];
		t_key = key->cross_reference_output_key;
		// If cross-reference-output-key is same or the corresponding table/column is same, an xref has already been
		// generated in this physical plan so skip doing the check again for whether it has been generated or not.
		if ((NULL == t_key) || (prev_t_key == t_key)
			|| ((NULL != prev_t_key) && (prev_t_key->table == t_key->table)
							&& (prev_t_key->column == t_key->column))) {
			continue;
		}
		prev_t_key = t_key;
		UNPACK_SQL_STATEMENT(value, t_key->table->tableName, value);
		tableName = value->v.reference;
		UNPACK_SQL_STATEMENT (value, t_key->column->columnName, value);
		columnName = value->v.reference;
		%{}DO:'$DATA(^{{ config->global_names.raw_octo }}("xref_status","{{ tableName }}","{{ columnName }}")) {}%
		%{}^{{ t_key->cross_reference_filename }}(cursorId)`n{{ PLAN_LINE_START }}{}%
	}
	%{}NEW %%ydboctoz{}%
	limit_keyword = get_keyword_from_keywords(plan->keywords, OPTIONAL_LIMIT);
	if ((NULL != limit_keyword) && (NULL == plan->dnf_prev)) {
		%{} SET %%ydboctozlimit({{ plan->outputKey->unique_id|%d }})=0{}%
	}
	if (NULL == plan->dnf_prev) {
		%{} KILL {}% TMPL(tmpl_key, plan->outputKey); %{}{}%
	}
	%{}`n{{ PLAN_LINE_START }}{}% // Whitespace for MUMPS
	cur_key = 0;
	dot_count = 0;
	assert(FALSE == plan->tablejoin_body_group_by_done);
	TMPL(tmpl_tablejoin, plan, plan->tablejoin, cur_key, FALSE, dot_count, tableName, columnName);
	/* If this plan has GROUP BY, "tmpl_tablejoin" would not have generated output keys in the usual format.
	 * It would have generated GROUP BY related subscript lvn tree that needs to be converted into an output key.
	 * Do that here.
	 */
	if (IS_GROUP_BY_PLAN(plan) && (NULL == plan->dnf_next)) {
		assert(FALSE == plan->tablejoin_body_group_by_done);
		plan->tablejoin_body_group_by_done = TRUE;
		TMPL(tmpl_group_by, plan, dot_count);
		%{}`n{{ PLAN_LINE_START }}{}%
		%{}KILL {}%
		TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
		%{},{{ GROUP_BY_SUBSCRIPT }}){}%
	}
	// If this expression had an order by, we now need to go back and make the ordering uniform
	// If we have a lot of plans each of which have the same output key and order by, then do this step
	// only for the last plan in that set.
	if ((NULL != plan->order_by) && (NULL == plan->dnf_next)) {
		char			*direction;
		LogicalPlan		*order_by;
		int			num_cols;

		order_by = plan->order_by;
		%{}`n{{ PLAN_LINE_START }}{}%
		%{}NEW %%ydboctoi  SET %%ydboctoi(0)=1`n{{ PLAN_LINE_START }}{}%
		assert((NULL != plan->outputKey) && ((NULL == plan->next) || (NULL != plan->next->outputKey)));
		// Determine how many columns are specified in ORDER BY. Need to generate M code accordingly.
		// The below code is a simplified version of that in "tmpl_column_list_combine.ctemplate".
		num_cols = 0;
		do {
			boolean_t	is_desc;

			assert(LP_ORDER_BY == order_by->type);
			TMPL(tmpl_print_dots, num_cols);
			num_cols++;
			%{}SET %%ydboctoi({{ num_cols|%d }})="" {}%
			is_desc = (OPTIONAL_DESC == order_by->extra_detail.lp_order_by.direction);
			direction = (is_desc ? "-1" : "1");
			/* In case of ASCENDING order, we want an empty string subscript (if any) to show up first
			 * whereas for DESCENDING order, we want it to show up last. Handle it accordingly below.
			 */
			if (is_desc) {
				%{}FOR  SET %%ydboctoi({{ num_cols|%d }})=$ORDER({}%
				TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
				TMPL(tmpl_order_by_key, num_cols);
				%{}),{{ direction }})  QUIT:$DATA({}%
				TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
				TMPL(tmpl_order_by_key, num_cols);
				%{}))=0  DO  QUIT:(%%ydboctoi({{ num_cols|%d }})=""){}%
			} else {
				%{}FOR  DO:$DATA({}%
				TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
				TMPL(tmpl_order_by_key, num_cols);
				%{}))  SET %%ydboctoi({{ num_cols|%d }})=$ORDER({}%
				TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
				TMPL(tmpl_order_by_key, num_cols);
				%{}),{{ direction }})  QUIT:(%%ydboctoi({{ num_cols|%d }})=""){}%
			}
			if (NULL != limit_keyword) {
				TMPL(tmpl_limit_check, limit_keyword, "!", "<%ydboctoi(0)");
			}
			%{}`n{{ PLAN_LINE_START }}{}%
			order_by = order_by->v.lp_default.operand[1];
		} while (NULL != order_by);
		TMPL(tmpl_print_dots, num_cols);
		num_cols++;
		%{}SET %%ydboctoi({{ num_cols|%d }})="" {}%
		%{}FOR  SET %%ydboctoi({{ num_cols|%d }})=$ORDER({}%
		TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
		TMPL(tmpl_order_by_key, num_cols);
		%{})) QUIT:(%%ydboctoi({{ num_cols|%d }})=""){}%
		if (NULL != limit_keyword) {
			TMPL(tmpl_limit_check, limit_keyword, "!", "<%ydboctoi(0)");
		}
		%{}  DO`n{{ PLAN_LINE_START }}{}%
		TMPL(tmpl_print_dots, num_cols);
		if (!plan->stash_columns_in_keys) {
			%{}SET {}%
			TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
			%{},%%ydboctoi(0))={}%
			TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
			TMPL(tmpl_order_by_key, num_cols);
		} else {
			/* Plan is part of a sub-query with an ORDER BY. Finish actual stashing of keys in columns
			 * that was deferred in "tmpl_populate_output_key".
			 */
			%{}IF $INCREMENT({}%
			TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
			%{},{}%
			TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
			TMPL(tmpl_order_by_key, num_cols);
			%{})){}%
		}
		%{})`n{{ PLAN_LINE_START }}{}%
		TMPL(tmpl_print_dots, num_cols);
		%{}IF $INCREMENT(%%ydboctoi(0)){}%
		%{}`n{{ PLAN_LINE_START }}{}%
		%{}KILL {}%
		TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
		%{},{{ ORDER_BY_SUBSCRIPT }}){}%
		%{}`n{{ PLAN_LINE_START }}SET {}%
		TMPL(tmpl_key, plan->outputKey); (*buffer_index)--;
		%{})=%%ydboctoi(0)-1{}%
	}
	if (NULL == plan->dnf_next) {
		if (plan->emit_duplication_check) {
			%{}`n{{ PLAN_LINE_START }}{}%
			%{}KILL %%ydboctozduplicate({{ plan->outputKey->unique_id|%d }}){}%
		}
		if (NULL != limit_keyword) {
			%{}`n{{ PLAN_LINE_START }}{}%
			%{}KILL %%ydboctozlimit({{ plan->outputKey->unique_id|%d }}){}%
		}
		if (plan->distinct_values) {
			%{}`n{{ PLAN_LINE_START }}{}%
			%{}KILL %%ydboctozdistinct({{ plan->outputKey->unique_id|%d }}){}%
		}
	}
	set_oper = plan->set_oper_list;
	prev_oper = set_oper;
	assert((NULL == set_oper) || (NULL == set_oper->prev));
	for ( ; NULL != set_oper; )
	{
		prev_oper = set_oper;
		set_oper = set_oper->next;
	}
	for ( ; NULL != prev_oper; )
	{
		switch(prev_oper->set_oper_type)
		{
			case LP_SET_UNION:
				plan_helper_mlabref = (plan->stash_columns_in_keys ? "columnkeyUNION" : "UNION");
				break;
			case LP_SET_UNION_ALL:
				plan_helper_mlabref = (plan->stash_columns_in_keys ? "columnkeyUNIONALL" : "UNIONALL");
				break;
			case LP_SET_EXCEPT:
				plan_helper_mlabref = (plan->stash_columns_in_keys ? "columnkeyEXCEPT" : "EXCEPT");
				break;
			case LP_SET_EXCEPT_ALL:
				plan_helper_mlabref = (plan->stash_columns_in_keys ? "columnkeyEXCEPTALL" : "EXCEPTALL");
				break;
			case LP_SET_INTERSECT:
				plan_helper_mlabref = (plan->stash_columns_in_keys ? "columnkeyINTERSECT" : "INTERSECT");
				break;
			case LP_SET_INTERSECT_ALL:
				plan_helper_mlabref = (plan->stash_columns_in_keys ? "columnkeyINTERSECTALL" : "INTERSECTALL");
				break;
			default:
				plan_helper_mlabref = NULL;
				assert(FALSE);
				break;
		}
		if (NULL != plan_helper_mlabref) {
			%{}`n{{ PLAN_LINE_START }}DO {{ plan_helper_mlabref }}^%%ydboctoplanhelpers({}%
			%{}{{ prev_oper->input_id1|%d }},{{ prev_oper->input_id2|%d }},{{ prev_oper->output_id|%d }}){}%
		}
		prev_oper = prev_oper->prev;
	}
	%{}`n{{ PLAN_LINE_START }}QUIT{}%
	TEMPLATE_END();
}
%{}
